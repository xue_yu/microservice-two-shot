import React from "react";
import { Link } from 'react-router-dom';


class ShoeList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shoesArray: [],
        };
        this.handleDelete = this.handleDelete.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/shoes/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                // Get the list of shoes
                const data = await response.json();
                const requests = [];
                for (let shoe of data.shoes) {
                    const detailUrl = `http://localhost:8080${shoe.href}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);
                const shoesArray = [];
                for (const shoesResponse of responses) {
                    if (shoesResponse.ok) {
                        const details = await shoesResponse.json();
                        shoesArray.push(details);
                    } else {
                        console.error(shoesResponse);
                    }
                }

                this.setState({ shoesArray: shoesArray });
            }
        } catch (e) {
            console.error(e);
        }
    }

    async handleDelete(shoes, event) {
        event.preventDefault();

        const url = `http://localhost:8080${shoes.href}`;
        const deleteResponse = await fetch(url, { method: 'delete' })
        if (deleteResponse.ok) {
            this.componentDidMount()
        }
    }

    render() {
        return (
            <div className="container">
                <h2>Shoes details list</h2>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-left">
                    <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add a pair of shoes</Link>
                </div>
                <div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Manufacture</th>
                                <th>Name</th>
                                <th>Color</th>
                                <th>Closet</th>
                                <th>Bin Number</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.shoesArray.map((shoes) => {
                                return (
                                    <tr key={shoes.href} >
                                        <td >
                                            <img src={shoes.picture_url} />
                                        </td>
                                        <td>
                                            {shoes.manufacturer}</td>
                                        <td >
                                            {shoes.name}
                                        </td>
                                        <td >
                                            {shoes.color}
                                        </td>

                                        <td >
                                            {shoes.bin.closet_name}
                                        </td>
                                        <td>
                                            {shoes.bin.bin_number}
                                        </td>
                                        <td>
                                            <button onClick={this.handleDelete.bind(this, shoes)}>Delete</button>
                                        </td>
                                    </tr>

                                );
                            }
                            )}
                        </tbody>
                    </table>
                </div>
            </div>

        );

    }
}

export default ShoeList;

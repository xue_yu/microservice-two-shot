import { Link } from 'react-router-dom';
import React from 'react';


class HatsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hatsArray: [],
        };
        this.handleDelete = this.handleDelete.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/hats/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                // Get the list 
                const data = await response.json();

                // Create a list of for all the requests and
                // add all of the requests to it
                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090${hat.href}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);
                const hatsArray = [];
                for (const hatsResponse of responses) {
                    if (hatsResponse.ok) {
                        const details = await hatsResponse.json();
                        hatsArray.push(details);
                    } else {
                        console.error(hatsResponse);
                    }
                }

                this.setState({ hatsArray: hatsArray });
            }
        } catch (e) {
            console.error(e);
        }
    }
    async handleDelete(hats, event) {
        event.preventDefault();

        const url = `http://localhost:8090${hats.href}`;
        const deleteResponse = await fetch(url, { method: 'delete' })
        if (deleteResponse.ok) {
            this.componentDidMount()
        }
    }

    render() {
        return (
            <div className="container">
                <h2>All The Hats</h2>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-left">
                    <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add hats</Link>
                </div>
                <div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Style</th>
                                <th>Fabric</th>
                                <th>Color</th>
                                <th>closet</th>
                                <th>Section Number</th>
                                <th>Picture</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.hatsArray.map((hats) => {
                                return (
                                    <tr key={hats.href}>
                                        <td>{hats.style_name}</td>
                                        <td>{hats.fabric}</td>
                                        <td>{hats.hat_color}</td>
                                        <td>{hats.location.closet_name}</td>
                                        <td>{hats.location.section_number}</td>
                                        <td>{<img src={hats.hat_url} alt="" width="100px" height="100px" />} </td>
                                        <td> <button onClick={this.handleDelete.bind(this, hats)}>Delete</button> </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }

}

export default HatsList; 

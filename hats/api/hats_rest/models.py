from django.db import models

# Create your models here.
from django.urls import reverse

from django.conf import settings

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, blank=True, null=True, unique=True)
    closet_name = models.CharField(max_length=100, default="000000", null=True,blank=True)
    section_number = models.PositiveSmallIntegerField(default=0, blank=True, null=True)
    shelf_number = models.PositiveSmallIntegerField(default=0, blank=True, null=True)


class Hats(models.Model):
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    hat_color = models.CharField(max_length=100)
    hat_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name= "hats",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"pk": self.pk})
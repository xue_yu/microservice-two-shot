from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True, blank=True)
    closet_name = models.CharField(
        max_length=100, default="0000000", null=True, blank=True
    )
    bin_number = models.PositiveSmallIntegerField(default=0, null=True, blank=True)
    bin_size = models.PositiveSmallIntegerField(default=0, null=True, blank=True)


# Create your models here.
class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})
